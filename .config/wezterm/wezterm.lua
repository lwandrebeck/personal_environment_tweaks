local wezterm = require 'wezterm'
local mux = wezterm.mux
local config = {}
config.font = wezterm.font 'SauceCodePro Nerd Font'

wezterm.on('gui-startup', function(cmd)
  -- allow `wezterm start -- something` to affect what we spawn
  -- in our initial window
  local tab, first_pane, window = mux.spawn_window(cmd or {})
  window:gui_window():maximize()
  -- local _, first_pane, window = window:spawn_tab {}
  local _, second_pane, window = window:spawn_tab {}
  local _, third_pane, window = window:spawn_tab {}
  first_pane:send_text "ssh user@host\n"
  second_pane:send_text "z\n"
  third_pane:send_text "ssh user@host\n"
  first_pane:activate()
  local args = {}
  if cmd then
    args = cmd.args
  end
end)

return config
